// https://eslint.org/docs/user-guide/configuring

module.exports = {
  root: true,
  parserOptions: {
    'ecmaVersion': 2019,
    'ecmaFeatures': {
      'jsx': true
    },
    'sourceType': 'module'
  },
  env: {
    'browser': false,
    'commonjs': true,
    'es6': true,
    'node': true
  },
  extends: [
    'eslint:recommended',
  ],
  // required to lint *.vue files
  plugins: [
  ],
  // add your custom rules here
  rules: {
    'no-const-assign': 'warn',
    'no-this-before-super': 'warn',
    'no-undef': 'warn',
    'no-unreachable': 'warn',
    'no-unused-vars': 'warn',
    'constructor-super': 'warn',
    'valid-typeof': 'warn',
    'no-multiple-empty-lines': ['warn', { 'max': 1 }],
    // allow async-await
    'no-new': 0,
    'generator-star-spacing': 'off',
    // allow debugger during development
    // 'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off'
  }
};
