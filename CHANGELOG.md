v1.0.7 - September 14, 2019
* Added more documentation and update default values in `addiesaas-frontend-forge.json` 

v1.0.6 - September 14, 2019
* Fix `addiesaas-frontend-forge.json`

v1.0.5 - September 14, 2019
* Change method to get installed path

v1.0.4 - September 14, 2019
* Change method to get installed path

v1.0.3 - September 13, 2019
* Update readme

v1.0.2 - September 13, 2019
* Update readme

v1.0.1 - September 13, 2019
* Initial release with support for init and install

