# AddieSaaS Frontend Forge

CLI tool to help developers to easily forge a development setup for RocketEffect Frontend containers. It build's a development environment by adding [RocketEffect Frontend Core](https://bitbucket.org/addiesaas/frontend-core/src/master/) and desired frontend container(s) in a single project. 
 
 >  Read [RocketEffect Frontend Core documentation](https://bitbucket.org/addiesaas/frontend-core/src/master/) for more information on how Frontend Core works.

## Installation

This is a CLI too and better installed globally.

`npm install addiesaas-frontend-forge --global` 

or

`npm i addiesaas-frontend-forge -g`


## Usage

1. Open terminal/cmd 
2. Go to an empty directory
3. Run `addiesaas-frontend-forge init`
4. This create a `addiesaas-frontend-forge.json` file 
5. Update this file to add container name(s) and setup other properties
6. Run `addiesaas-frontend-forge install`

## `addiesaas-frontend-forge.json` properties

```json
{
  "team": "addiesaas",
  "vcs": "git@bitbucket.org",
  "app": {
    "repo": "git@bitbucket.org:addiesaas/frontend-core.git",
    "branch": "master",
  },
  "containers": {
    "container-client-need2speed-frontend": "master"
  },
  "options": {
    "env-create": true,
    "npm-install": true,
    "npm-run-dev": false,
    "env": {
      "APP_ENV": "local",
      "PARENT_HOST": "https://dev.rocketeffect.com",
      "PARENT_JS_PATH":"/js/wp-plugin.js",
      "CACHE": "reload",
      "VUE_DIST": "vue/dist/vue.esm.js"
    }
  },
  "config": {

  }
}
```

* `app`  - Refers to the git repo of **Rocket Effect Frontend core** package along with branch

* `containers` - Is an object of container names (keys) and respective branch name (value). For details, see [More Information](#container-names) section on container names.

* `options`
    * `env-create` - If set to `true`, creates `.env` file and fills with default variables. These can be overwritten by variables defined in `env` object. For details, see [More Information](#env-variables) section on Environment Variables.
    * `npm-install` - If set to `true` runs `npn install`
    * `npm-run-dev` - If set to `true` runs `npn run dev-silent` 
    
* `config` - For future use.

## More information

### <a id="container-names"></a>Container Names

A Rocket Effect containers is a single git repo. The name here refers to the repository name. For example, if the repository url is `git@bitbucket.org:addiesaas/container-client-need2speed-frontend.git` the name would be `container-client-need2speed-frontend`
 

### <a id="env-variables"></a>Environment Variables

**PARENT_HOST**: Url of the RocketEffect installation that hosts the parent `wp-plugin.js`. For more details on PARENT_HOST, read [Frontend Core documentation](https://bitbucket.org/addiesaas/frontend-core/src/master/)

**PARENT_JS_PATH**: Path of wp-plugin.js in the PARENT_HOST. It's usually, `/js/wp-plugin.js`. For more details on PARENT_JS_PATH, read [Frontend Core documentation](https://bitbucket.org/addiesaas/frontend-core/src/master/)

