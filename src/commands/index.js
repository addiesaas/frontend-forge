const _ = require('lodash');
const {writeLog, terminateOnError} = require('../lib/process');
const {whiteBold} = require('../lib/colors');
const {commandize} = require('../lib/fs');
const {version, installedPath} = require('../lib/info');

const cli = require('../lib/cli')(async (cli) => {
  cli.version(version);

  let scripts = {}
  try {
    scripts = require(`${installedPath}/scripts.json`) || {}
  } catch (e) {
    terminateOnError(e, 'Error locating command definitions!', '')
  }

  /* eslint no-unused-vars: off */
  _.forOwn(scripts, async ({description = '', options = {}, ...otherOptions}, commandName) => {
    options = options || {}
    const executable = commandize(options.executable || commandName)
    if (executable) {
      cli.command(commandName)
        .description(description)
        .action(async () => {
          try {
            const command = require(`${executable}`)
            await command.call(cli)
          } catch (e) {
            terminateOnError(e, `Unable to run ${executable} command`, '')
          }
        });
    }
  })

  cli.on('--help', function () {
    writeLog('')
    writeLog('Steps:', 'green.bold');
    writeLog('  $  addiesaas-frontend-forge init', 'green.bold');
    writeLog('  $  addiesaas-frontend-forge install', 'green.bold');
  });

  //show help by default
  if (!process.argv.slice(2).length) {
    cli.outputHelp(whiteBold);
  }

});

module.exports = cli
