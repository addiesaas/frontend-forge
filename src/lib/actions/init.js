const {terminateOnError, writeLog} = require('../process')
const {path, fs, currentPath} = require('../fs');
const {installedPath} = require('../info');
const {prompt} = require('../inquirer');
const {configFile} = require('../config');

const createConfigJSON = () => {
  writeLog('');
  const dest = path.resolve( `${currentPath}/${configFile}`);
  const source = path.resolve(`${installedPath}/data/${configFile}`);

  if (fs.pathExistsSync(dest)) {
    writeLog('Config file already exists: ' + dest, 'yellow.bold');
    writeLog('You can overwrite it or exit.', 'yellow.bold');
    prompt([{
      type: 'confirm',
      name: 'isOverwrite',
      message: 'Do you want to overwrite?',
      default: false
    }]).then(({isOverwrite}) => {
        if (isOverwrite) {
          copyFile(source, dest)
        } else {
          writeLog('');
          writeLog('Terminating init.', 'red.bold');
        }
      })
  } else {
    copyFile(source, dest)
  }
};

const copyFile = (source, dest) => {
  try {
    writeLog('Creating config file: ' + dest);
    fs.copySync(source, dest)
    writeLog('');
    writeLog('Done! You can now go ahead and edit the file and run `install`', 'white.bold');
  } catch (e) {
    terminateOnError(e,'Unable to create config file: ' + dest, 'init')
  }
};

module.exports = createConfigJSON;
