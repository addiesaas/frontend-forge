const _ = require('lodash');
const {terminateOnError, writeLog} = require('../process');
const {path, currentPath} = require('../fs');
const {cloneAndCopyRepo} = require('../git');

const postInstall = require('./post-install-update')

const {configFile, appRepoPath, containersRepoPath, containerRepoPrefix} = require('../config');

const containerRepoTempPath = path.resolve(`./${containersRepoPath}`);

const cleanup = true;
const installer = async () => {

  let config = {}

  try {
    config = require(path.resolve(`${currentPath}/${configFile}`));
  } catch (e) {
    terminateOnError(e, 'Unable to read config file', 'installation')
  }
  let app = config.app
  if (!app) {
    terminateOnError({config}, 'Unable to get main repository', 'installation')
  }
  if (!_.isObject(app)) {
    app = {
      repo: app,
      branch: 'master'
    }
  }
  const appRepo = app.repo

  const gitOptions = ['-b', app.branch || 'master'];

  try {
  await writeLog('Start installation');
  }catch(e) {
    terminateOnError(e, 'Error occurred while creating main application', 'installation')
  }

  config = config || {};
  if (_.isEmpty(config.containers || {})) {
    writeLog('Frontend containers are not defined! Will install Main Application only.', 'yellow');
  }

  writeLog('');
  writeLog('Setting up Main Application');
  try {
    await cloneAndCopyRepo(appRepo, appRepoPath, {gitOptions, cleanup})
  } catch (e) {
    terminateOnError(e, 'Error occurred while creating main application', 'installation')
  }
  writeLog('Done.');

  const containers = config.containers || {};
  const containerNames = (Object.keys(containers))
  for(let i = 0 ; i < containerNames.length ; i ++) {
    let repoName = containerNames[i]
    let branch = containers[repoName]

    writeLog('');
    writeLog('Creating container for ' + repoName);
    let repoUrl = containerRepoPrefix + repoName

    try {
    let repoPath = path.resolve(containerRepoTempPath + '/' + repoName)
    let gitOptions = ['-b', branch || 'master']
    await cloneAndCopyRepo(repoUrl, repoPath, {
      cleanup,
      repoName,
      gitOptions,
      cloneCallback: async (repoUrl1, repoPath1, options) => {
        const composer = require(`${repoPath1}/composer.json`);
        const pack = _.get(composer, 'extra.addiesaas.package', {});
        const {name: packName = 'Undefined', section: packSection = 'UndefinedSection'} = pack;
        options.destination = path.resolve(`${currentPath}/src/${packSection}/${packName}`)
      }
    })

    await postInstall(config)

    } catch (e) {
      terminateOnError(e, 'Error occurred while creating container - ' + repoName, 'installation')
    }
  }

  writeLog('');
  writeLog('');
  writeLog('End installation');
}

module.exports = installer;
