const _ = require('lodash');
const {terminateOnError, writeLog, execExternal} = require('../process');
const {saveEnv, createEnv} = require('../env');
const  {series} = require('async');

const updater = async (config) => {

  const options = config.options || {}

  if (options['env-create']) {
    writeLog('');
    writeLog('Create .env file');
    await createEnv()
  }
  if (!_.isEmpty(options['env'])) {
    writeLog('');
    writeLog('Update env variables');
    await saveEnv(options.env || {})
  }

  const externalCommands = [];
  if (options['npm-install']) {
    externalCommands.push((callback) => {
      const cmd = execExternal('npm', ['install'])
      cmd.on('exit', () => {
         if (_.isFunction(callback)) callback()
      })
    })
    writeLog('Prepare for npm install','gray');
  }
  if (options['npm-run-dev']) {
    externalCommands.push((callback) => {
      const cmd = execExternal('npm', ['run', 'dev-silent'])
      cmd.on('exit', () => {
        if (_.isFunction(callback)) callback()
      })
    })
    writeLog('');
    writeLog('Prepare for npm run dev', 'gray');
  }
  if (externalCommands.length) {
    writeLog('');
    try {
      await series(externalCommands);
    } catch (e) {
      terminateOnError(e, 'Error', 'External')
    }

    writeLog('');
  }
}

module.exports = updater;
