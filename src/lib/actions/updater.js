const {terminateOnError, writeLog} = require('../process');
const {path, currentPath} = require('../fs');
const {configFile} = require('../config');

const postInstall = require('./post-install-update')

const updater = async () => {

  let config = {}

  try {
    config = require(path.resolve(`${currentPath}/${configFile}`));
  } catch (e) {
    terminateOnError(e, 'Unable to read config file', 'installation')
  }

  await postInstall(config)

  writeLog('');
  writeLog('');
  writeLog('End Update');
}

module.exports = updater;
