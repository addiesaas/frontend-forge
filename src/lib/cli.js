// https://github.com/tj/commander.js/
const _ = require('lodash');
const commander = require('commander');

module.exports = (callback) => {
  const cli = new commander.Command();
  if (_.isFunction(callback)) callback(cli);
  cli.parse(process.argv);
  return cli
}
