const _ = require('lodash')
const colors = require('colors');

const allColors = {colors}
allColors.whiteBold = (txt) => colors.bold.white(txt)
allColors.greenBold = (txt) => colors.bold.green(txt)
allColors.colorize = (txt, styles = '') => (_.get(colors, styles))(txt)
allColors.redBold = (txt) => colors.red.bold(txt)

module.exports = allColors
