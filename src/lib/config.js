
module.exports = {
  configFile: 'addiesaas-frontend-forge.json',
  cachePath: '.cache',
  repoPath: '.cache/repos',
  appRepoPath: '.cache/repos/app',
  containersRepoPath: '.cache/repos/containers',
  containerRepoPrefix: 'git@bitbucket.org:addiesaas/',
  spawn: {
    options: {
      detached: true
    }
  }
}
