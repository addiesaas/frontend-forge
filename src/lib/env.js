const {path, fs} = require('./fs');
const envfile = require('envfile')
const defaultEnvPath = '.env'

const saveEnv = (object, fileName = '') => {
  const envPath = path.resolve(fileName || defaultEnvPath)
  fs.ensureFileSync(envPath)
  const env = envfile.parseFileSync(envPath)
  const newEnv = envfile.stringifySync(Object.assign(env || {}, object))
  fs.writeFileSync(envPath, newEnv);
}

const createEnv = async (filename = '', sourceName = '.env.example') => {
  const envPath = filename || defaultEnvPath
  fs.ensureFileSync(path.resolve(sourceName))
  fs.copySync(path.resolve(sourceName), path.resolve(envPath), {overwrite: true})
}

module.exports = {saveEnv, createEnv}
