const path = require('path');
const fs = require('fs-extra');
const {installedPath} = require('../lib/info');
const tar = require('tar');

const allFs = {
  path,
  fs,
  tar,
  currentPath: path.resolve('./'),
  exists: resource => fs.pathExistsSync(path.resolve(resource)),
  commandPath: command => installedPath + '/src/commands/' + command,
  commandExists: command => allFs.exists(allFs.commandPath(command)),
  commandize: command => allFs.commandExists(command) && allFs.commandPath(command),
  moveDirHere: async (src, options) => {
    options = options || {}
    const {destination = null, cache = '.cache', basename = (new Date()).valueOf()} = options
    const tempTar = options.tempTar || `${cache}/${basename}.tgz`;

    const createOption = Object.assign({
      sync: true,
      gzip: true,
      preservePaths: false,
      cwd: src,
      file: tempTar,
    }, options.tarCreateOptions || {})

    await tar.c(createOption, [`./`])

    const extractOptions = Object.assign({
      sync: true,
      file: tempTar,
    }, options.tarExtractOptions || {})
    if (destination) {
      fs.ensureDirSync(destination)
      extractOptions.C = destination
    }
    await tar.x(extractOptions)

    // cleanup
    if (options.cleanup) {
      fs.removeSync(path.resolve(`${tempTar}`))
    }
  }
}

module.exports = allFs;
