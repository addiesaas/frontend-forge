const _ = require('lodash')
const {writeLog} = require('./process')
const {fs, path, moveDirHere} = require('./fs');

const git = require('simple-git/promise');

const cloneRepo = async (repo, tempPath, options) => {
  options = options || {options}
  const resolvedTempPath = path.resolve(tempPath)
  const {gitOptions = ['-b', 'master']} = options
  // cleanup
  fs.emptyDirSync(resolvedTempPath)
  writeLog('Cloning ' + repo + ' ' + gitOptions.join(' '), 'yellow')
  await git(resolvedTempPath).clone(repo, resolvedTempPath, gitOptions)

}

const cloneAndCopyRepo = async (repo, tempPath, options) => {
  options = options || {options}

  await cloneRepo(repo, tempPath, options)
  if (_.isFunction(options.cloneCallback)) {
    await options.cloneCallback(repo, tempPath, options)
  }

  const {repoName = 'app', cache = '.cache'} = options
  /* eslint require-atomic-updates: off */
  options.tempTar = (options.tempTar || `${cache}/${repoName}.tgz`);
  await moveDirHere(tempPath, options)
  if (_.isFunction(options.moveCallback)) {
    await options.moveCallback(repo, tempPath, options)
  }

  if (options.cleanup) {
    const resolvedTempPath = path.resolve(tempPath)
    fs.emptyDirSync(resolvedTempPath)
  }
}

module.exports = {git, cloneAndCopyRepo}
