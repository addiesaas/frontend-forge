require('pkginfo')(module);
const { getInstalledPathSync } = require('get-installed-path')

let installedPath = '';
try {
  installedPath = getInstalledPathSync(module.exports.name);
} catch (e) {
  const path = require('path');
  installedPath = path.resolve(__dirname + '/../..');
}

const appRoot = module.exports.appRoot = require('app-root-path');
module.exports.appPath  =appRoot.toString();
module.exports.installedPath = installedPath;
