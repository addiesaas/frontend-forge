const _ = require('lodash')
const {colorize} = require('./colors');
const {exec, spawn} = require('child_process');
const {platform} = require('os');
const isWindows = platform() === 'win32';
const {spawn: execOptions = {}} = require('./config')

const execExternal = (command, args = [], options = {}, config = {}) => {
  options = Object.assign(execOptions, options || {})
  if (isWindows) {
    options.shell = true;
  }
  const stdCallback = config.stdout || ((message) => writeLog(message, 'blue.bold'))
  const stdErrCallback = config.stderr || ((message) => writeLog(message, 'red.bold'))
  const endCallback = config.stdend || ((message) => writeLog(message, 'green.bold.inverse'))

  endCallback("\n" + command + ' ' + args.join(' ') + "\n", 'green.bold.inverse')
  const cmd = spawn(command, args, options);

  cmd.stdout.on('data', (data) => {
    stdCallback(data.toString());
  })
  cmd.stderr.on('data', (data) => {
    stdErrCallback(data.toString());
  })
  cmd.on('exit', (code) => {
    if (code !== 0) {
      return stdErrCallback('Error! Code: ' + code)
    }
    endCallback("\n" + 'Completed ' + command + ' ' + args.join(' ') + "\n", 'green.bold.inverse');
  })

  return cmd
};

const writeLog = (message, colors = 'cyan') => {
  if (colors) {
    message = colorize(message, colors)
  }
  console.log(message)
};

const terminateOnError = (e, message, processType, beforeTerminate, afterTerminate) => {
  if (_.isFunction(beforeTerminate)) {
    /* eslint no-undef: off */
    beforeTerminate(...arguments)
  }
  writeLog(message, 'red.bold.inverse');
  writeLog(e.message, 'red');
  writeLog(e, 'red');

  writeLog(`Terminating ${processType}.`, 'red.bold.inverse');

  if (_.isFunction(afterTerminate)) {
    /* eslint no-undef: off */
    afterTerminate(...arguments)
  }
  process.exit(1);
}

const thisProcess = {
  spawn,
  exec,
  execExternal,
  writeLog,
  terminateOnError
}

module.exports = thisProcess
